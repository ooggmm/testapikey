﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace WebApi.Attributes
{
    [AttributeUsage(validOn: AttributeTargets.Class | AttributeTargets.Method)]
    public class ApiKeyAttribute : Attribute, IAsyncActionFilter
    {
        private const string API_KEY_NAME = "X-API-KEY";
        private const string CONFIG_API_KEY_NAME = "ReportingServiceApiKey";
        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            var appSettings = context.HttpContext.RequestServices.GetRequiredService<IConfiguration>();
            var apiKey = appSettings.GetValue<string>(CONFIG_API_KEY_NAME);

            if (!context.HttpContext.Request.Headers.TryGetValue(API_KEY_NAME, out var headerApiKey) | !apiKey.Equals(headerApiKey))
            {
                context.Result = new ContentResult()
                {
                    StatusCode = 401,
                    Content = "Invalid Api Key"
                };
                return;
            }

            await next();
        }


    }
}
